﻿# Both values use the distinguished name
$OrganizationalUnit = ''
$DesiredGroupDistinguishedName = ''


Import-Module activeDirectory


# Verify that variables are provided
if ($OrganizationalUnit -eq '') {
    Do {
        Write-Host "Which organizational unit would you like to verify? (Distinguished Name)"
        $OrganizationalUnit = Read-Host
    } Until ($OrganizationalUnit -ne '')
}
if ($DesiredGroupDistinguishedName -eq '') {
    Do {
        Write-Host "Which security group would you like added organizational group members? (Distinguished Name)"
        $DesiredGroupDistinguishedName = Read-Host
    } Until ($DesiredGroupDistinguishedName -ne '')
}


Write-Host "Gathering user groups..."
try { 
    # gather Group object
    $DesiredGroup = Get-ADGroup -Identity $DesiredGroupDistinguishedName
    
    # gather all users in ou
    $Users = Get-ADUser -Filter * -SearchBase $OrganizationalUnit -Properties * 

    # gather group information
    foreach ($User in $Users) {
        $CurrentGroups = Get-ADPrincipalGroupMembership $User.SamAccountName | select name, distinguishedName


        # verify 'msft agents' group existance
        $CorrectGroupStatus = $false
        foreach ($Group in $CurrentGroups) {
            if ($Group.distinguishedName -eq 'CN=msft agents,OU=Agents,OU=MSFT,OU=Utah,DC=eawphx,DC=edatwork,DC=com') { 
                $CorrectGroupStatus = $True 
                Write-Host "User in correct group:" $User.SamAccountName
            }
        }

        # correct missing group (if necessary)
        if ($CorrectGroupStatus -eq $false) {
            Write-Host -ForegroundColor Yellow "User NOT in correct group:" $User.SamAccountName
            [console]::beep(500,700)

            # prompt user for confirmation of group add
            Write-Host "Should" $User.SamAccountName "be added to" $DesiredGroup.name "? (Y/N)"
            $Confirmation = Read-Host

            if ($Confirmation -eq 'y' -or $Confirmation -eq 'yes') {
                # add user to group using the group object, and user object
                Add-ADGroupMember -Identity $DesiredGroup -Members $User

                Write-Host "Group addition complete"
            }
            else {
                Write-Host "Ignoring user" $User.SamAccountName
                continue
            }

        }
    }
}
catch { 
    Write-Host "An error gathering groups occurred, and the script must exit"
    pause
    exit
}
